﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PirategameUnleashed
{


    public class Ship
    {
        private string name = "";

        private Blip destination = null;
        private Blip startPoint = null;

        private int capacity = 0;
        private double speed = 0;

        private Blip currentBlip;

        private Queue<Blip> journey;
        private Stack<Blip> elapsedJourney;
        public Ship(Blip currentBlip)
        {
            this.currentBlip = currentBlip;
        }

        public void Update(GameTime gameTime)
        {
            this.speed += (double)gameTime.ElapsedGameTime.Milliseconds / 1000;
            if (speed >= 1)
            {
                speed = 0;
                //Debug.WriteLine("Moved, current Blip: "+currentBlip.getColumn() + ","+currentBlip.getRow());

                this.currentBlip.makeSea();
                this.currentBlip = journey.Dequeue();
                this.currentBlip.makeShip();

                elapsedJourney.Push(this.currentBlip);
                if (journey.Count == 0)
                {
                    while (elapsedJourney.Count > 0)
                    {
                        journey.Enqueue(elapsedJourney.Pop());
                    }
                    elapsedJourney.Clear();
                }
            }
        }

        public void setJourney(Queue<Blip> newJourney)
        {
            this.journey = newJourney;
            this.elapsedJourney = new Stack<Blip>();
        }

    }
}
